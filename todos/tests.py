from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse

# Create your tests here.
from todos.models import Todo


class TodosAPITestCase(APITestCase):
    def authenticate(self):
        self.client.post(reverse('register'), data={"email": "tester@domain.com",
                                                    "username": "tester",
                                                    "password": "12345#ABc!12345"})

        response = self.client.post(reverse('login'),
                                    data={"email": "tester@domain.com", "password": "12345#ABc!12345"})
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {response.data['token']}")

    def create_todo(self):
        sample_todo = {
            "title": "Third Todo",
            "desc": "My Third Todo Desc"
        }
        response = self.client.post(reverse('todos'), data=sample_todo)
        return response


class TestListCreateTodos(TodosAPITestCase):

    def test_should_not_create_todo_with_no_auth(self):
        # reverse here translate the name into the url provided in todos.urls.py
        response = self.create_todo()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_should_create_todo(self):
        self.authenticate()
        previous_todo_count = Todo.objects.all().count()
        response = self.create_todo()
        self.assertEqual(Todo.objects.all().count(), previous_todo_count + 1)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], 'Third Todo')
        self.assertEqual(response.data['desc'], 'My Third Todo Desc')

    def test_retrieves_all_todos(self):
        self.authenticate()
        response = self.client.get(reverse('todos'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response.data['results'], list)
        self.create_todo()
        res = self.client.get(reverse('todos'))
        self.assertIsInstance(res.data['count'], int)
        self.assertEqual(res.data['count'], 1)


class TestTodoDetail(TodosAPITestCase):

    def test_retrieves_one_item(self):
        self.authenticate()
        response = self.create_todo()
        res = self.client.get(reverse('todo', kwargs={'id': response.data['id']}))
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        todo = Todo.objects.get(id=response.data['id'])
        self.assertEqual(todo.title, res.data['title'])

    def test_updates_one_item(self):
        self.authenticate()
        response = self.create_todo()
        res = self.client.patch(reverse('todo', kwargs={'id': response.data['id']}),
                                data={"title": "Updated Todo", "is_complete": True})
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        updated_todo = Todo.objects.get(id=response.data['id'])
        self.assertEqual(updated_todo.is_complete, True)
        self.assertEqual(updated_todo.title, "Updated Todo")

    def test_deletes_one_item(self):
        self.authenticate()
        res = self.create_todo()
        prev_db_count = Todo.objects.all().count()


        self.assertGreater(prev_db_count, 0)
        self.assertEqual(prev_db_count, 1)

        response = self.client.delete(reverse('todo', kwargs={'id': res.data['id']}))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Todo.objects.all().count(), 0)
