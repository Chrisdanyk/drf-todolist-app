from django.db import models

# Create your models here.
from django.db.models import CASCADE

from authentication.models import User
from helpers.models import TrackingModel


class Todo(TrackingModel):
    title = models.CharField(max_length=255, blank=False, null=False)
    desc = models.TextField(null=True)
    is_complete = models.BooleanField(default=False)
    owner = models.ForeignKey(to=User, on_delete=CASCADE)

    def __str__(self):
        return self.title
